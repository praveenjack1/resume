import { Avatar, Card, Checkbox, Divider, Grid, List, ListItem, ListItemIcon, ListItemText, Typography } from '@mui/material'
import { Box, Container, Stack, width } from '@mui/system'
import React from 'react'
import image from '../assets/img.png'
import EmailIcon from '@mui/icons-material/Email';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import CircleRoundedIcon from '@mui/icons-material/CircleRounded';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

const Home = () => {
    return (
        <Box>
            <Container sx={{ my: 1 }}>
                
                        <Card sx={{ boxShadow: "0 0 25px 0 rgb(0 0 0 / 18%)", p: 5, borderRadius: 2, }}>
                            <Grid container alignItems={"center"}>
                                <Grid xs={9}>
                                    <Typography sx={{ fontWeight: 600 ,}} variant="h3"  >Prakash Meiyanathan</Typography>
                                    <Typography variant='h4' sx={{ fontWeight: 600, color: '#009688' }}>MERN Stack Developer</Typography>
                                    <Stack sx={{ rowGap: "2px", mt: 1 }}>
                                        <Stack direction={"row"} spacing={1} >
                                            <EmailIcon style={{ fill: "#009688" }} />
                                            <Typography sx={{ color: "#999", fontSize: "medium" }}>praveenmc2000@gmail.com</Typography>
                                        </Stack>
                                        <Stack direction={"row"} spacing={1}>
                                            <LocationOnIcon style={{ fill: "#009688" }} />
                                            <Typography sx={{ color: "#999", fontSize: "medium" }}>Chennai,TamilNadu, India</Typography>
                                        </Stack>
                                        <Stack direction={"row"} spacing={1}>
                                            <LocalPhoneIcon style={{ fill: "#009688" }} />
                                            <Typography sx={{ color: "#999", fontSize: "medium" }}>9025971628</Typography>
                                        </Stack>
                                    </Stack>
                                </Grid>
                                <Grid xs={3} sx={{ justifyContent: "end", display: "flex" }}>
                                    {/* <Avatar src={image} sx={{width:"160px",height:"160px"}} /> */}
                                    <Card sx={{ boxShadow: "0 0 25px 0 rgb(0 0 0 / 8%)", borderRadius: "50%", width:"160px", height: "160px", display: "flex", alignItems: "center", justifyContent: "center", border: "5px solid #009688 ", bgcolor: "#c6ced0" }}>
                                        <img src={image} width="85%" />
                                    </Card>
                                </Grid>     
                            </Grid>

                            <Stack sx={{ mt: 6 }}>
                                <Typography variant='h5' sx={{ fontWeight: 600, color: '#009688' }}>CAREER OBJECTIVE</Typography>
                                <Divider sx={{ borderBottom: "3px solid #009688 " }} />
                                <List>
                                    <ListItem alignItems="flex-start">
                                        <ListItemIcon sx={{ minWidth: "28px" }}>
                                            <FiberManualRecordIcon style={{ fontSize: "medium" }} />
                                        </ListItemIcon>
                                        <ListItemText
                                            secondary={
                                                <React.Fragment>
                                                    <Typography sx={{ color: "#999", fontSize: "medium" }}> Self-motivated, highly passionate and hardworking fresher looking for an opportunity to work in a challenging organization to utilize my skills and knowledge to work for the growth of the organisation.</Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                    <ListItem alignItems="flex-start">
                                        <ListItemIcon sx={{ minWidth: "28px" }}>
                                            <FiberManualRecordIcon style={{ fontSize: "medium" }} />
                                        </ListItemIcon>
                                        <ListItemText
                                            secondary={
                                                <React.Fragment>
                                                    <Typography sx={{ color: "#999", fontSize: "medium" }}>A highly technical and knowledgeable Software Engineer, seeking an entrylevel position in an organization that offers good growth prospects. Have an
                                                        internship experience of 3 months and advanced knowledge of programming
                                                        as well as UI and UX designing</Typography>                                                        </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                </List>
                            </Stack>
                            <Stack >
                                <Typography variant='h5' sx={{ fontWeight: 600, color: '#009688' }}> Skills</Typography>
                                <Divider sx={{ borderBottom: "3px solid #009688 " }} />

                                <Grid container mt={2} spacing={3}>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>Backend</Typography>
                                        <Stack >
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>Node JS</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>Web Technologies</Typography>
                                        <Stack >
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>
                                                React JS, Redux, JavaScript, TypeScript, HTML5, CSS, SCSS/SASS
                                            </Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>Database</Typography>
                                        <Stack >
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>MySQL, Mongo DB</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>Code Versioning</Typography>
                                        <Stack>
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>Git Hub, Git Lab</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>FrameWorks</Typography>
                                        <Stack>
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>Material-UI,  Bootstrap , Semantic UI</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>Package Management</Typography>
                                        <Stack>
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>npm , yarn</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Typography variant='h6' sx={{ fontWeight: 600, color: '#009688', textDecoration: "underline" }}>Novice</Typography>
                                        <Stack>
                                            <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>PHP</Typography>
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </Stack>
                            <Grid container spacing={4} mt={1}>
                                <Grid item xs={7}>
                                    <Typography variant='h5' sx={{ fontWeight: 600, color: '#009688' }}>EDUCATION </Typography>
                                    <Divider sx={{ borderBottom: "3px solid #009688 " }} />
                                    <Stack sx={{ mt: 2 }}>
                                        <Typography sx={{ fontWeight: 600, fontSize: "14px" }}> BSc IT (Information Technology) </Typography>
                                        <Typography sx={{ fontWeight: 600, fontSize: "14px", color: '#009688' }}> 63%</Typography>
                                        <Grid container>
                                            <Grid item xs={3.5}>
                                                <Stack direction={"row"} spacing={1}>
                                                    <CalendarMonthIcon sx={{ fill: "#999", fontSize: "medium" }} />
                                                    <Typography sx={{ color: "#999", fontSize: "medium" }}>2017-2020</Typography>

                                                </Stack>
                                            </Grid>
                                            <Grid xs={8.5}>
                                                <Stack direction={"row"} spacing={1}>
                                                    <LocationOnIcon sx={{ fill: "#999", fontSize: "medium" }} />
                                                    <Typography sx={{ color: "#999", fontSize: "medium", }}>Mahatma Arts and Science College - Pudukottai</Typography>
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                        <Divider sx={{ mt: "10px" }} />
                                    </Stack>
                                    <Stack sx={{ mt: 2 }}>
                                        <Typography sx={{ fontWeight: 600, fontSize: "14px" }}> HSC (12th) </Typography>
                                        <Typography sx={{ fontWeight: 600, fontSize: "14px", color: '#009688' }}> 60%</Typography>
                                        <Grid container>
                                            <Grid item xs={3.5}>
                                                <Stack direction={"row"} spacing={1}>
                                                    <CalendarMonthIcon sx={{ fill: "#999", fontSize: "medium" }} />
                                                    <Typography sx={{ color: "#999", fontSize: "medium" }}>2017</Typography>

                                                </Stack>
                                            </Grid>
                                            <Grid xs={8.5}>
                                                <Stack direction={"row"} spacing={1}>
                                                    <LocationOnIcon sx={{ fill: "#999", fontSize: "medium" }} />
                                                    <Typography sx={{ color: "#999", fontSize: "medium", }}>GOVT.HR.SEC. School - Maramadakki</Typography>
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                        <Divider sx={{ mt: "10px" }} />
                                    </Stack>
                                    <Stack sx={{ mt: 2 }}>
                                        <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>SSLC (10th) </Typography>
                                        <Typography sx={{ fontWeight: 600, fontSize: "14px", color: '#009688' }}> 80%</Typography>
                                        <Grid container>
                                            <Grid item xs={3.5}>
                                                <Stack direction={"row"} spacing={1}>
                                                    <CalendarMonthIcon sx={{ fill: "#999", fontSize: "medium" }} />
                                                    <Typography sx={{ color: "#999", fontSize: "medium" }}>2015</Typography>

                                                </Stack>
                                            </Grid>
                                            <Grid xs={8.5}>
                                                <Stack direction={"row"} spacing={1}>
                                                    <LocationOnIcon sx={{ fill: "#999", fontSize: "medium" }} />
                                                    <Typography sx={{ color: "#999", fontSize: "medium", }}>GOVT.HR.SEC. School - Maramadakki</Typography>
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                        <Divider sx={{ mt: "10px" }} />
                                    </Stack>
                                </Grid>
                                <Grid item xs={5}>
                                    <Typography variant='h5' sx={{ fontWeight: 600, color: '#009688' }}>LANGUAGES</Typography>
                                    <Divider sx={{ borderBottom: "3px solid #009688 " }} />
                                    <Stack sx={{ mt: 2 }}>
                                        <Grid container alignItems={"center"}>
                                            <Grid item xs={4}>
                                                <Typography sx={{ fontWeight: 600, fontSize: "large" }}> Tamil</Typography>
                                            </Grid>
                                            <Grid xs={8}>
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Typography sx={{ fontWeight: 600, fontSize: "large" }}> English</Typography>
                                            </Grid>
                                            <Grid xs={8}>
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                                <Checkbox {...label} icon={<CircleRoundedIcon sx={{ fill: "#a5d4fe" }} />} checkedIcon={<CircleRoundedIcon sx={{ fill: "#009688" }} />} />
                                            </Grid>
                                        </Grid>
                                    </Stack>
                                </Grid>
                            </Grid>
                            <Stack sx={{ mt: 3 }}>
                                <Typography variant='h5' sx={{ fontWeight: 600, color: '#009688' }}>INTERESTS</Typography>
                                <Divider sx={{ borderBottom: "3px solid #009688 " }} />
                                <Stack sx={{ mt: 2 }}>
                                    {/* <Typography sx={{ color: "#999", fontSize: "medium" }}>I hereby declare that the above-furnished details are true and to the best of my knolwedge</Typography> */}
                                    <Typography sx={{ fontWeight: 600, fontSize: "14px" }}> Cricket</Typography>
                                    <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>programming</Typography>

                                </Stack>
                            </Stack>
                        </Card>
            </Container>
        </Box >
    )
}

export default Home