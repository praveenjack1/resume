import logo from './logo.svg';
import './App.css';
import Home from './page/Home';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Prakash from './components/prakash';


const theme = createTheme({
  typography: {
    fontFamily: 'Poppins, sans-serif !important',
  },
})

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Home />
        {/* <Prakash /> */}
      </div>
    </ThemeProvider>
  );
}

export default App;
